file(GLOB_RECURSE sources CONFIGURE_DEPENDS
        src/*.c
        src/*.h
        include/*.h
)

add_executable(image-transformer ${sources}
        src/reading_checkpoint.c
        include/reading_checkpoint.h
        src/writing_checkpoint.c
        include/writing_checkpoint.h
        src/bmp_reading.c
        include/bmp_reading.h
        include/bmp_header.h
        src/bmp_writing.c
        include/bmp_writing.h
)
target_include_directories(image-transformer PRIVATE src include)
