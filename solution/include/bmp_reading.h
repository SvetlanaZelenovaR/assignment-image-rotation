#ifndef IMAGE_TRANSFORMER_BMP_READING_H
#define IMAGE_TRANSFORMER_BMP_READING_H
#pragma once

#include "bmp_header.h"
#include "image.h"

#include <stdio.h>
#include <stdlib.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_HEADER,
    READ_INVALID_FILE,
    MEMORY_ALLOCATING_ERROR
};

enum read_status from_bmp(FILE* input, struct image** img);

#endif
