#ifndef IMAGE_TRANSFORMER_BMP_WRITING_H
#define IMAGE_TRANSFORMER_BMP_WRITING_H
#pragma once

#include "bmp_header.h"
#include "image.h"

#include <stdio.h>
#include <stdlib.h>

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_header(FILE* output, const struct image* img, uint8_t* pad);

enum write_status to_data(FILE* output, const struct image* img, uint8_t padding);

enum write_status to_bmp(FILE* output, const struct image* img);

#endif
