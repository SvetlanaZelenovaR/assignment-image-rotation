#ifndef IMAGE_TRANSFORMER_READING_CHECKPOINT_H
#define IMAGE_TRANSFORMER_READING_CHECKPOINT_H
#pragma once

#include "../include/bmp_reading.h"
#include "image.h"

#include <stdlib.h>

enum read_status reading_checkpoint(FILE* input, struct image** img);

#endif
