#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATING_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATING_H
#pragma once

#include "image.h"

#include <stdio.h>
#include <stdlib.h>

struct image* rotation(struct image* const src);

#endif
