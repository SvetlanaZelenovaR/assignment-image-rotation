#ifndef IMAGE_TRANSFORMER_WRITING_CHECKPOINT_H
#define IMAGE_TRANSFORMER_WRITING_CHECKPOINT_H
#pragma once

#include "../include/bmp_writing.h"
#include "image.h"

#include <stdlib.h>

enum write_status writing_checkpoint(FILE* output, struct image** img, struct image** rotated_img);

#endif
