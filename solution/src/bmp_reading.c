#include "../include/bmp_reading.h"

enum read_status from_bmp(FILE* input, struct image** img) {
    // Проверяем успешность открытия исходного изображения
    if (input == NULL) {
        perror("Error occurred while opening source file");
        return READ_INVALID_FILE;
    }
    const uint16_t kBmpType = 0x4D42;
    // Чтение заголовка BMP файла
    struct bmp_header header;
    // Проверяем успешность чтения заголовка BMP файла
    uint64_t response = fread(&header, sizeof(struct bmp_header), 1, input);
    if (response != 1)
        return READ_INVALID_HEADER;

    // Проверка сигнатуры BMP файла
    if (header.bfType != kBmpType)
        return READ_INVALID_SIGNATURE;

    // Создание пустого изображения
    *img = new_image(header.biWidth, header.biHeight);
    if (*img == NULL)
        return MEMORY_ALLOCATING_ERROR;

    // Рассчитываем размер строки без учета отступа
    uint64_t row_length = (*img)->width * sizeof(struct pixel);
    // Вычисляем необходимый отступ
    uint8_t padding = (4 - (row_length % 4)) % 4;

    // Чтение пикселей с учетом отступа
    for (uint64_t y = 0; y < header.biHeight; ++y) {
        fread(&(*img)->data[y * header.biWidth], sizeof(struct pixel), header.biWidth, input);

        // Пропускаем отступ, так как он содержит ненужные байты
        // Аргумент SEEK_CUR отвечает за отсчет padding байтов от текущей позиции
        fseek(input, (int) padding, SEEK_CUR);
    }
    return READ_OK;
}
