#include "../include/bmp_writing.h"

enum write_status to_header(FILE* output, const struct image* img, uint8_t* pad) {
    // Запись заголовка BMP файла
    const uint16_t kBmpType = 0x4D42;
    struct bmp_header header;
    size_t header_size = sizeof(struct bmp_header);
    size_t pixel_size = sizeof(struct pixel);
    header.bfType = kBmpType;

    // Рассчитываем размер строки с учетом отступа
    uint64_t row_length = img->width * pixel_size;
    // Вычисляем необходимый отступ
    *pad = (4 - (row_length % 4)) % 4;
    // Вычисляем округленный размер изображения
    header.bfileSize = header_size + (row_length + *pad) * img->height;
    // заполняем заголовок BMP файла
    header.bfReserved = 0;
    header.bOffBits = header_size;
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    // Проверяем успешность записи заголовка BMP файла
    uint64_t response = fwrite(&header, header_size, 1, output);
    if (response != 1)
        return WRITE_ERROR;
    return WRITE_OK;
}

enum write_status to_data(FILE* output, const struct image* img, uint8_t padding) {
    size_t pixel_size = sizeof(struct pixel);
    uint64_t response;
    // Запись пикселей с учетом отступа
    for (uint64_t y = 0; y < img->height; ++y) {
        response = fwrite(&img->data[y * img->width], pixel_size, img->width, output);
        if (response != img->width) {
            return WRITE_ERROR;
        }
        // Добавляем отступ
        for (uint8_t p = 0; p < padding; ++p) {
            response = fputc(0, output);
            if (response == EOF) {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE* output, const struct image* img) {
    if (output == NULL) {
        return WRITE_ERROR;
    }
    uint8_t padding;
    enum write_status status = to_header(output, img, &padding);
    if (status != WRITE_OK) {
        return status;
    }
    status = to_data(output, img, padding);
    if (status != WRITE_OK) {
        return status;
    }
    return WRITE_OK;
}
