#include "../include/image.h"
#include <stdlib.h>

struct image* new_image(uint64_t width, uint64_t height) {
    // Создаем указатель на новый объект image и выделяем на него память
    struct image* img = (struct image*) malloc(sizeof(struct image));
    // Проверяем, что выделение памяти прошло успешно
    if (img == NULL) {
        return NULL;
    }
    img->width = width;
    img->height = height;
    img->data = (struct pixel*) malloc(width * height * sizeof(struct pixel));
    // Проверяем, что выделение памяти прошло успешно
    if (img->data == NULL) {
        free(img);
        return NULL;
    }
    return img;
}

void delete_image(struct image* img) {
    // Освобождаем память занятую изображением
    free(img->data);
    free(img);
}
