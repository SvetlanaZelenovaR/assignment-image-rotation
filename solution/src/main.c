#include "../include/reading_checkpoint.h"
#include "../include/rotation.h"
#include "../include/writing_checkpoint.h"

int main(__attribute__((unused)) int argc, char* argv[]) {
    char* source_picture = argv[1];
    char* result_picture = argv[2];

    struct image* picture = NULL;
    // Открываем исходное изображение для чтения в бинарном режиме
    FILE* input = fopen(source_picture, "rb");
    // Создаем новую структуру картинки, проверяя наличие ошибок
    enum read_status rs = reading_checkpoint(input, &picture);
    // Проверяем успешность чтения исходного изображения
    if (rs != READ_OK) {
        perror("Error occurred while reading source file");
        // освобождаем занятую память
        delete_image(picture);
        return rs;
    }

    // Поворачиваем картинку, проверяя наличие ошибок
    struct image* rotated_picture = rotation(picture);

    // Открываем новый файл для записи транспонированного изображения в бинарном режиме
    FILE* output = fopen(result_picture, "wb");
    // Записываем результат, проверяя наличие ошибок
    enum write_status ws = writing_checkpoint(output, &picture, &rotated_picture);
    // Проверяем успешность записи в результирующее изображение
    if (ws != WRITE_OK) {
        perror("Error occurred while creating rotated image");
        return ws;
    }

    return EXIT_SUCCESS;
}
