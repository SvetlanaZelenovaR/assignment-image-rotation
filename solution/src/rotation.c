#include "../include/rotation.h"

struct image* rotation(struct image* const src) {
    uint64_t old_height = src->height, old_width = src->width;
    // Создаем новое изображение с транспонированной шириной и высотой
    struct image* res = new_image(old_height, old_width);
    if (res == NULL) {
        perror("Error occurred while creating new image");
        delete_image(res);
        exit(EXIT_FAILURE);
    }
    uint64_t new_width = res->width;
    // Производим поворот картинки, записывая пиксели на их новую позицию
    for (uint64_t y = 0; y < old_height; ++y) {
        for (uint64_t x = 0; x < old_width; ++x) {
            res->data[(x + 1) * new_width - 1 - y] = src->data[y * old_width + x];
        }
    }
    return res;
}
