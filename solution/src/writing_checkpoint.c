#include "writing_checkpoint.h"

enum write_status writing_checkpoint(FILE* output, struct image** img, struct image** rotated_img) {
    // Записываем результирующее изображение в файл
    enum write_status write_response = to_bmp(output, *rotated_img);

    // освобождаем память занятую исходным и результирующим изображением
    delete_image(*img);
    delete_image(*rotated_img);
    return write_response;
}
